import { Catch, ExceptionFilter, ArgumentsHost } from "@nestjs/common";
import { LibraryBusinessError } from "./library-business.error";
import { Response } from "express";
import { ErrorType } from "src/models/enums/error-type";

@Catch(LibraryBusinessError)
export class GlobalExceptionFilter implements ExceptionFilter {
    catch(exception: LibraryBusinessError, host: ArgumentsHost) {

        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();

        switch (exception.errorType) {
            case ErrorType.NotBorrowed: {
                response
                    .status(405)
                    .json({
                        message: 'Method Not Allowed',
                        description: exception.message
                    });
                break;
            }
            case ErrorType.NotReturned: {
                response
                    .status(405)
                    .json({
                        message: 'Method Not Allowed',
                        description: exception.message
                    });
                break;
            }
            case ErrorType.NoReview: {
                response
                    .status(405)
                    .json({
                        message: 'Method Not Allowed',
                        description: exception.message
                    });
                break;
            }
            case ErrorType.BookNotFound: {
                response
                    .status(404)
                    .json({
                        message: 'Not Found',
                        description: exception.message
                    });
                break;
            }
            case ErrorType.BookNotBorrowedByUser: {
                response
                    .status(403)
                    .json({
                        message: 'Forbidden',
                        description: exception.message
                    });
                break;
            }
            case ErrorType.BookNotBorrowed: {
                response
                    .status(405)
                    .json({
                        message: 'Method Not Allowed',
                        description: exception.message
                    });
                break;
            }
            case ErrorType.BookNotFree: {
                response
                    .status(405)
                    .json({
                        message: 'Method Not Allowed',
                        description: exception.message
                    });
                break;
            }
            case ErrorType.ReviewNotFound: {
                response
                    .status(404)
                    .json({
                        message: 'Not Found',
                        description: exception.message
                    });
                break;
            }

            case ErrorType.ReviewAlreadyDeleted: {
                response
                    .status(405)
                    .json({
                        message: 'Method Not Allowed',
                        description: exception.message
                    });
                break;
            }

            case ErrorType.UserAlreadyExist: {
                response
                    .status(405)
                    .json({
                        message: 'Method Not Allowed',
                        description: exception.message
                    });
                break;
            }

            case ErrorType.UserNotFound: {
                response
                    .status(405)
                    .json({
                        message: 'Method Not Allowed',
                        description: exception.message
                    });
                break;
            }

            case ErrorType.ReviewNotFound: {
                response
                    .status(404)
                    .json({
                        message: 'Not Found',
                        description: exception.message
                    });
                break;
            }


            default: {
                response
                    .status(500)
                    .json({
                        message: 'Internal server error',
                        description: exception.message
                    });
                break;
            }

        }
    }

}