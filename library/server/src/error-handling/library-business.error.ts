import { ErrorType } from "src/models/enums/error-type";

export class LibraryBusinessError {
    constructor(
        message: string,
        errorType: ErrorType
    ) {
        this.message = message;
        this.errorType = errorType;
    }

    message: string;
    errorType: ErrorType
}
