import { Controller, Get, Post, Body, Put, Param, UseGuards, ValidationPipe, Req } from '@nestjs/common';
import { ReviewsService } from 'src/services/reviews.service';
import { VoteService } from 'src/services/vote.service';
import { CreateVoteDTO } from 'src/dtos/voteDtos/create-vote.dto';
import { BlacklistGuard } from 'src/auth/blacklist.guard';

@Controller('reviews')
export class ReviewsController {
    private readonly reviewsService: ReviewsService;
    private readonly voteService: VoteService;
    constructor(
        reviewsService: ReviewsService,
        voteService: VoteService
    ) {
        this.reviewsService = reviewsService;
        this.voteService = voteService;
    };

    
    @Get(':id/votes')
    @UseGuards(BlacklistGuard)
    async getVotes(
        @Param('id') id: string
    ) {
        return await this.voteService.getReviewVotes(+id);
    };

    
    @Post(':id/votes')
    @UseGuards(BlacklistGuard)
    async voteAReview(
        @Param('id') id: string,
        @Req() request: any,
        @Body(new ValidationPipe({ whitelist: true })) vote: CreateVoteDTO
        ): Promise<any> {            
            return await this.voteService.voteReview(+id, vote, request);
        };
        
        @Put(':id/votes')
        @UseGuards(BlacklistGuard) 
        async updateVote(
            @Param('id') id: string,
            @Req() request: any,
            @Body(new ValidationPipe({ whitelist: true })) vote: CreateVoteDTO
        ): Promise<any> {
            return await this.voteService.updateVote(+id, vote, request);
        };


}
