import { Controller, Post, UseGuards, Body, ValidationPipe, Param, Delete, Get, UseInterceptors, UploadedFile } from '@nestjs/common';
import { CreateUserDTO } from 'src/dtos/userDtos/create-user.dto';
import { UsersService } from 'src/services/users.service';
import { BlacklistGuard } from 'src/auth/blacklist.guard';
import { RolesGuard } from 'src/auth/roles-guard';
import { UserRole } from 'src/models/enums/user-role';
import { UserBanDTO } from 'src/dtos/userDtos/user-ban.dto';
import { ReturnUserDTO } from 'src/dtos/userDtos/user.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage} from 'multer'
import { extname } from 'path';

@Controller('users')
export class UsersController {
    constructor(
        private readonly userService: UsersService,
    ) { }

    @Get()
    async getAll(): Promise<ReturnUserDTO[]> {
      return await this.userService.getAll();
    }

    @Post()
    async createUser(
        @Body(new ValidationPipe({ whitelist: false })) newUser: CreateUserDTO
    ): Promise<ReturnUserDTO> {
        return await this.userService.create(newUser);
    }

    @Post(':id/avatar')
    @UseInterceptors(
        FileInterceptor('files', {
          storage: diskStorage({
            destination: './avatars',
            filename: (req, file, cb) => {
              // Generating a 32 random chars long string
              console.log('filename cb', file);
              const randomName = Array(32)
                .fill(null)
                .map(() => Math.round(Math.random() * 16).toString(16))
                .join('');
              //Calling the callback passing the random name generated with the original extension name
              cb(null, `${randomName}${extname(file.originalname)}`);
            },
          }),
        }),
      )
    
    async uploadAvatar(
        @UploadedFile() files,
        @Param('id') id: string,
    ) {
        console.log(files);
        return await this.userService.uploadAvatar(+id, files.filename)
    }

    @Post(':id/ban')
    @UseGuards(BlacklistGuard, new RolesGuard(UserRole.admin))
    async banUser(
        @Param('id') userId: string,
        @Body(new ValidationPipe({ whitelist: true })) banDTO: UserBanDTO) {
          console.log(banDTO)
        return await this.userService.banUser(+userId, banDTO.period);
    }

    @Delete(':id')
    @UseGuards(BlacklistGuard, new RolesGuard(UserRole.admin))
    async deleteUser(
        @Param('id') userId: string
    ) {
        return await this.userService.deleteUser(+userId);
    }
}

