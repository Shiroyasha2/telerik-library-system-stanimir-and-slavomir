import { Controller, Get, Param, Put, Delete, Post, Body, UseGuards, Req, ValidationPipe } from '@nestjs/common';
import { BooksService } from 'src/services/books.service';
import { ReviewsService } from 'src/services/reviews.service';
import { CreateReviewDTO } from 'src/dtos/reviewDtos/create-review.dto';
import { BlacklistGuard } from 'src/auth/blacklist.guard';
import { ReturnReviewDTO } from 'src/dtos/reviewDtos/return-review.dto';
import { ReturnBookDTO } from 'src/dtos/bookDtos/return-book.dto';
import { ReturnBookWithBorrowedDTO } from 'src/dtos/bookDtos/book-with-borrowed.dto';
import { ReturnBookWithRatingFromUserDTO } from 'src/dtos/bookDtos/book-with-rating-from-user.dto';
import { ReturnBookWithRatingDTO } from 'src/dtos/bookDtos/book-with-rating.dto';

@Controller('books')
export class BooksController {
    constructor(
        private readonly booksService: BooksService,
        private readonly reviewsService: ReviewsService
        ) { }

    @Get()
    // @UseGuards(BlacklistGuard)
    async getAll(
        // @Req() request: any
    ): Promise<ReturnBookDTO[]> {
        // console.log('Req from books.controller'); 
        return await this.booksService.getAll();
    }

    @Get(':id')
    @UseGuards(BlacklistGuard)
    async getBookById(@Param('id') bookId: string): Promise<ReturnBookDTO> {
        return await this.booksService.getBookById(+bookId);
    }

    @Put(':id')
    @UseGuards(BlacklistGuard)
    async borrowBook(
        @Param('id') bookId: string,
        @Req() request: any,
        ): Promise<ReturnBookWithBorrowedDTO> {
            const userId = request.user.id
            return await this.booksService.borrowBook(+bookId, userId)
    }

    @Delete(':id')
    @UseGuards(BlacklistGuard)
    async returnBook(
        @Param('id') bookId: string,
        @Req() request: any,
        ): Promise<ReturnBookDTO> {
            const userId = request.user.id
        return await this.booksService.returnBook(+bookId, userId)
    }

    @Put('/:id/:rate')
    @UseGuards(BlacklistGuard)
    async rateBook(
        @Param('id') bookId: string,
        @Param('rate') rating: string,
        @Req() request: any,
    ): Promise<ReturnBookWithRatingDTO> {
        const userId = request.user.id
        return await this.booksService.rateBook(+bookId, +rating, userId)
    }

    @Get('/:bookId/rate/:userId')
    async getBookRateByUser(
        @Param('bookId') bookId: string,
        @Param('userId') userId: string
    ): Promise<ReturnBookWithRatingFromUserDTO> {
        return await this.booksService.getBookRateByUser(+userId, +bookId );
    }
    
    @Get(':id/reviews')  
    @UseGuards(BlacklistGuard)
    async getBookReview(
        @Param('id') bookId: string,
        @Req() request: any,
        ): Promise<ReturnReviewDTO[]> {
        return await this.reviewsService.getReviewsByBookId(+bookId);
    }

    @Post(':id/reviews')
    @UseGuards(BlacklistGuard)
    async reviewBook(
        @Param('id') bookId: string,
        @Body(new ValidationPipe({whitelist: true})) reviewContent: CreateReviewDTO,
        @Req() request: any
    ): Promise<ReturnReviewDTO> {
        //console.log('Req from books.controller :id/reviews @Post', request);
        
        return await this.reviewsService.createReview(reviewContent, +bookId, request);
    }

    @Delete(':id/reviews/:reviewId')
    @UseGuards(BlacklistGuard)
    async deleteAReview(
        @Param('id') bookId: string,
        @Param('reviewId') reviewId: string,
        @Req() request: any
    ): Promise<ReturnReviewDTO> {
        return await this.reviewsService.deleteReview(+bookId, +reviewId, request);
    }
    @Put(':id/reviews/:reviewId')
    @UseGuards(BlacklistGuard)
        async updateAReview(
        @Param('id') bookId: string,
        @Param('reviewId') reviewId: string,
        @Body(new ValidationPipe({whitelist: false})) reviewContent: CreateReviewDTO,
        @Req() request: any
    ) {
        return await this.reviewsService.updateReview(+bookId, +reviewId, reviewContent, request);
    }
}
