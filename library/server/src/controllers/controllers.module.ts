import { Module } from '@nestjs/common';
import { BooksController } from './books.controller';
import { ServicesModule } from 'src/services/services.module';
import { UsersController } from './users.controller';
import { ReviewsController } from './reviews.controller';
import { AuthController } from './auth.controller';

@Module({
    imports: [ServicesModule],
    controllers: [UsersController, BooksController, ReviewsController, AuthController]})
export class ControllersModule {}
