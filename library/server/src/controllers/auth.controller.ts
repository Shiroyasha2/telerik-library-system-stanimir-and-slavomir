import { Controller, Post, Body, Delete } from '@nestjs/common';
import { AuthService } from 'src/services/auth.service';
import { UserLoginDTO } from 'src/dtos/userDtos/user-login-dto';
import { GetToken } from 'src/auth/get-token.decorator';

@Controller('session')
export class AuthController {
    
constructor(
    private readonly authService: AuthService,
) { }
@Post()
async login(@Body() user: UserLoginDTO):
Promise<{token: string}> {
    return await this.authService.login(user);
}

@Delete()
async logout(@GetToken() token: string): Promise<{ message: string}> {
    await this.authService.addToBlacklist(token?.slice(7))
    console.log('in server')
    
    return {
        message: 'You have been logged out!'
    }
}
}
