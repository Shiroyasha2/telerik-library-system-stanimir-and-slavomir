import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ControllersModule } from './controllers/controllers.module';
import { MulterModule } from '@nestjs/platform-express';


@Module({
  imports: [
    ControllersModule,
    MulterModule.register({
      dest: '/avatars',
    }),
    TypeOrmModule.forRoot({
      type: 'mariadb',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: 'root',
      database: 'test',
      entities: ["dist/**/*.entity{.ts,.js}"],
      synchronize: true
  }), 
  
  ],
})
export class AppModule {}
