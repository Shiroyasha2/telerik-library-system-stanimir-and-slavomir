import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { User } from "./users.entity";
import { Review } from "./reviews.entity";
import { ReviewReaction } from "./enums/reaction";

@Entity()
export class Vote {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'enum', enum: ReviewReaction, default: ReviewReaction.NoReaction })
    vote: ReviewReaction;
    
    @ManyToOne(
        () => User,
        user => user.votes
    )
    user: User;

    @ManyToOne(
        () => Review,
        review => review.id
    )
    review: Review;
}