import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToMany } from "typeorm";
import { Review } from "./reviews.entity";
import { Book } from "./books.entity";
import { Vote } from "./vote.entity";
import { Rate } from "./rate.entity";
import { UserRole } from "./enums/user-role";

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number;
    @Column()
    username: string;
    @Column()
    password: string;

    @Column({ type: 'enum', enum: UserRole, default: UserRole.guest })
    userRole: UserRole;
    @Column({ default: false })
    isDeleted: boolean;

    @OneToMany(
        () => Review,
        review => review.author
    )
    reviews: Review[];
    @OneToMany(
        () => Book,
        book => book.borrowedBy
    )
    borrowedBooks: Book[]

    @ManyToMany(
        () => Book,
        book => book.borrowHistory
    )
    bookHistory: Book[];


    @OneToMany(
        () => Vote,
        vote => vote.user
    )
    votes: Vote[];
    @OneToMany(
        () => Rate,
        rate => rate.user
    )
    rates: Rate[]
    @Column({ nullable: true })
    public avatarUrl: string;

    @Column({ nullable: true })
    banEndDate: Date;

}