import { Entity, PrimaryGeneratedColumn, ManyToOne, Column } from "typeorm";
import { User } from "./users.entity";
import { Book } from "./books.entity";
import { BookRating } from "./enums/book-rating";

@Entity()
export class Rate {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column({ type: 'enum', enum: BookRating, default: BookRating.noRating })
    rating: BookRating

    @ManyToOne(
        () => User,
        user => user.rates
    )
    user: User;

    @ManyToOne(
        () => Book,
        book => book.rate
    )
    book: Book;

}