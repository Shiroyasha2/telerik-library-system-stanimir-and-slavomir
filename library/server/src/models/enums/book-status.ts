export enum BookStatus {
    FREE = 1,
    BORROWED = 2,
    UNLISTED = 3,
}