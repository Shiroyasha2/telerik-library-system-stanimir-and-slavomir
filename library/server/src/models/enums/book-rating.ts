export enum BookRating {
    noRating = 1,
    didNotLikeIt = 2,
    itWasOkay = 3,
    likedIt = 4,
    reallyLikedIt = 5,
    itWasAmazing = 6,
}