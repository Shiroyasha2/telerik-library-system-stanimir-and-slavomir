export enum ReviewReaction {
    NoReaction = 1,
    Like = 2,
    Dislike = 3,
}