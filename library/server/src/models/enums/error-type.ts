export enum ErrorType {
    NotBorrowed,
    NotReturned,
    NoReview,
    BookNotFound,
    BookNotBorrowedByUser,
    BookNotBorrowed,
    BookNotFree,
    ReviewNotFound,
    ReviewAlreadyDeleted,
    UserAlreadyExist,
    ForeignAuthor,
    UserNotFound,
    
    
}
