import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne } from "typeorm";
import { User } from "./users.entity";
import { Book } from "./books.entity";
import { ReviewReaction } from "./enums/reaction";
import { Vote } from "./vote.entity";

@Entity()
export class Review {
    @PrimaryGeneratedColumn()
    id: number 
    
    @Column()
    content: string;

    @Column({ default: false })
    isDeleted: Boolean

    @Column({type: 'enum', enum: ReviewReaction, default: ReviewReaction.NoReaction})
    reaction: ReviewReaction;

    @ManyToOne(
        () => Book,
        book => book.review
    )
    book: Book;
    @ManyToOne(
        () => User,
        user => user.reviews
    )
    author: User;

    @OneToMany(
        () => Vote,
        vote => vote.review
    )
    votes: Vote[];

}