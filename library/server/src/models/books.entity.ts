import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne, ManyToMany, JoinTable } from "typeorm";
import { User } from "./users.entity";
import { Review } from "./reviews.entity";
import { BookStatus } from "./enums/book-status";
import { Rate } from "./rate.entity";

@Entity('books')
export class Book {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column()
    author: string;

    @Column({ default: BookStatus.FREE })
    status: BookStatus;

    @Column('float', {default: 0})
    rating: number;

    @OneToMany(
        () => Review,
        review => review.book
    )
    review: Review[];

    @ManyToMany(
        () => User,
        user => user.bookHistory
    )
    @JoinTable()
    borrowHistory: User[]
    @ManyToOne(
        () => User,
        user => user.borrowedBooks
    )
    borrowedBy: User;

    @OneToMany(
        () => Rate,
        rate => rate.book
    )
    rate: Rate[]

    @Column()
    coverUrl: string;

}

