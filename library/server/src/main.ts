import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { GlobalExceptionFilter } from './error-handling/global-exception.filter';
import * as express from 'express'
import { join } from 'path';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalFilters(new GlobalExceptionFilter());
  app.use('/avatars', express.static('avatars'))
  app.use('/covers', express.static('covers'))
  // app.use(express.static('./avatars/'))


  app.enableCors();
  await app.listen(3000);
}
bootstrap();
