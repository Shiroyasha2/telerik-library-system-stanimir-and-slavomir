import { Injectable } from '@nestjs/common';
import { ReturnBookDTO } from 'src/dtos/bookDtos/return-book.dto';
import { Book } from 'src/models/books.entity';
import { ReturnBookWithBorrowedDTO } from 'src/dtos/bookDtos/book-with-borrowed.dto';
import { ReturnBookWithRatingDTO } from 'src/dtos/bookDtos/book-with-rating.dto';
import { User } from 'src/models/users.entity';
import { ReturnUserDTO } from 'src/dtos/userDtos/user.dto';
import { Review } from 'src/models/reviews.entity';
import { ReturnReviewDTO } from 'src/dtos/reviewDtos/return-review.dto';
import { ReturnBookWithRatingFromUserDTO } from 'src/dtos/bookDtos/book-with-rating-from-user.dto';
import { ReturnFullUserDTO } from 'src/dtos/userDtos/return-full-user.dto';

@Injectable()
export class TransformService {
    public toBookDTO(book: Book): ReturnBookDTO {
        return {
            id: book.id,
            title: book.title,
            author: book.author,
            rating: book.rating,
            borrowedBy: book.borrowedBy && this.toReturnUserDTO(book.borrowedBy),
            coverUrl: book.coverUrl,
        }
    }

    public toBookWithBorrowed(book: Book): ReturnBookWithBorrowedDTO {
        return {
            title: book.title,
            author: book.author,
            borrowedBy: book.borrowedBy && this.toReturnUserDTO(book.borrowedBy)
        }
    }

    public toBookWithRating(book: Book): ReturnBookWithRatingDTO {
        return {
            id: book.id,
            title: book.title,
            author: book.author,
            rating: book.rating,
            borrowedBy: book.borrowedBy,
        }
    }
    public toBookWithRatingFromUser(book: Book, userId: number, ratingFromUser: number): ReturnBookWithRatingFromUserDTO {
        return {
            userId: userId,
            BookId: book.id,
            rating: ratingFromUser,
        }
    }

    public toReturnUserDTO(user: User): ReturnUserDTO {
        return {
            id: user.id,
            username: user.username,
            avatarUrl: user.avatarUrl,
        }
    }

    public toReturnFullUserDTO(user: User): ReturnFullUserDTO {
        return {
            id: user.id,
            username: user.username,
            avatarUrl: user.avatarUrl,
            isDeleted: user.isDeleted,
            banEndDate: user.banEndDate,
            role: user.userRole,
        }
    }

    public toReturnReviewDTO(review: Review): ReturnReviewDTO {
        return {
            content: review.content,
            forBook: review.book.title,
            id: review.id,
            author: review.author,
            votes: review.votes,
        }
    }
}
