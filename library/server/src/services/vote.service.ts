import { Injectable } from "@nestjs/common";
import { Repository } from "typeorm";
import { Vote } from "src/models/vote.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { ReviewsService } from "./reviews.service";
import { Review } from "src/models/reviews.entity";
import { CreateVoteDTO } from "src/dtos/voteDtos/create-vote.dto";
import { User } from "src/models/users.entity";

@Injectable()
export class VoteService {
    private votesRepository: Repository<Vote>;
    private reviewsRepository: Repository<Review>;
    private readonly reviewsService: ReviewsService;
    constructor(
        @InjectRepository(Vote) votesRepository: Repository<Vote>,
        @InjectRepository(Review) reviewsRepository: Repository<Review>
    ) {
        this.votesRepository = votesRepository;
        this.reviewsRepository = reviewsRepository;
    };

    async getReviewVotes(incommingReviewId: number): Promise<any> {
        const votes = await this.votesRepository.find({
            where: {
                review: incommingReviewId
            },
            relations: ['user', 'review'],
        });

        return votes;
    };

    async voteReview(incommingId: number, incommingVote: CreateVoteDTO, request: any): Promise<any> {
        const reviewToVote = await this.reviewsRepository.findOne({
            where: {
                id: incommingId,
            },
            relations: ['votes', 'votes.user'],
        });

        const incommingUser = request.user;
        const newVoteReview = new Vote();
        newVoteReview.vote = incommingVote.vote;
        newVoteReview.review = reviewToVote;
        newVoteReview.user = incommingUser;
        const savednewVoteReview = await this.votesRepository.save(newVoteReview);
        return savednewVoteReview;
    };

    async updateVote(incommingId: number, incommingVote: CreateVoteDTO, request: any): Promise<any> {
        const reviewToVote = await this.reviewsRepository.findOne({
            where: {
                id: incommingId,
            },
            relations: ['votes', 'votes.user'],
        });
        
        const voteToUpdate = await this.votesRepository.findOne({
            where: {
                user: request.user,
                review: reviewToVote, 
            },
            relations: ['user','review'],
        });
        voteToUpdate.vote = incommingVote.vote;

        const savedVote = await this.votesRepository.save(voteToUpdate);
        return savedVote;
        
    };

};