import { Injectable, UnauthorizedException, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/models/users.entity';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt'
import { UserLoginDTO } from 'src/dtos/userDtos/user-login-dto';
import { JWTPayload } from 'src/common/jwt-payload';
import { Repository } from 'typeorm';
import { UserRole } from 'src/models/enums/user-role';
import { Token } from 'src/models/token.entity';

@Injectable()
export class AuthService {
    constructor(
      @InjectRepository(User) private readonly userRepository: Repository<User>,
      @InjectRepository(Token) private readonly tokenRepository: Repository<Token>,
        private readonly jwtService: JwtService,
        ){}
    
      async findUserByName(username: string) {
        return await this.userRepository.findOne({ 
          where: {
            username,
            isDeleted: false,
          }
         });
      }
    
      async validateUser(username: string, password: string) {
        const user = await this.findUserByName(username);
        if (!user) {
          return null;
        }
        const isUserValidated = await bcrypt.compare(password, user.password);
        return isUserValidated
                ? user
                : null;
      }
    
      async login(loginUser: UserLoginDTO): Promise<{ token: string }> {
        const user = await this.validateUser(loginUser.username, loginUser.password);
    
        if (!user) {
          throw new UnauthorizedException('Wrong credentials!');
        }
    
        const payload: JWTPayload = {
          id: user.id,
          username: user.username,
          role: UserRole[user.userRole],
          avatarUrl: user.avatarUrl,
          banEndDate: user.banEndDate,
        }
    
        const token = await this.jwtService.signAsync(payload);
    
        return {
          token,
        };
      }

      async addToBlacklist(IncomingToken: string) {
        const foundToken = await this.tokenRepository.findOne({
          where: {
            token: IncomingToken,
          }
        })
        if (foundToken) {
          throw new BadRequestException('User is already logged out')
        }
        
        const tokenEntity = this.tokenRepository.create()
        tokenEntity.token = IncomingToken;
        await this.tokenRepository.save(tokenEntity)

        
      }

      async isBlacklisted(token: string): Promise<boolean> {
        return Boolean(await this.tokenRepository.findOne({
          where: {
            token,
          }
        }));
      }
}
