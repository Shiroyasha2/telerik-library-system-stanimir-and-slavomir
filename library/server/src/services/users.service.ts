import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/models/users.entity';
import { CreateUserDTO } from 'src/dtos/userDtos/create-user.dto';
import { ReturnUserDTO } from 'src/dtos/userDtos/user.dto';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt'
import { LibraryBusinessError } from 'src/error-handling/library-business.error';
import { ErrorType } from 'src/models/enums/error-type';
import { Review } from 'src/models/reviews.entity';
import { TransformService } from './transform.service';
import { Book } from 'src/models/books.entity';
import { BookStatus } from 'src/models/enums/book-status';

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
        @InjectRepository(Review) private readonly reviewsRepository: Repository<Review>,
        @InjectRepository(Review) private readonly booksRepository: Repository<Book>,

        private readonly transformService: TransformService,
    ) { }

    async getAll(): Promise<ReturnUserDTO[]> {
        const users = await this.usersRepository.find();
        return users.map(user => this.transformService.toReturnFullUserDTO(user));
    }

    async create(userDto: CreateUserDTO): Promise<ReturnUserDTO> {
        const checkForUserAlreadyExist = await this.usersRepository.findOne({
            where: {
                username: userDto.username,
            },
        });
        if (!checkForUserAlreadyExist) {
            const user = this.usersRepository.create(userDto);
            user.password = await bcrypt.hash(user.password, 10);

            const created = await this.usersRepository.save(user);

            return this.transformService.toReturnUserDTO(created);
        } else {
            throw new LibraryBusinessError(`User with name ${userDto.username} already exist!`, ErrorType.UserAlreadyExist);
        }
    }

    public async uploadAvatar(id: number, filename: string) {
        const user = await this.usersRepository.findOneOrFail(id);
        console.log(id)
        console.log(filename)

        user.avatarUrl = filename;

        await this.usersRepository.save(user);

        return this.transformService.toReturnUserDTO(user)

        // return {
        //   id: user.id,
        //   username: user.username,
        //   avatar: user.avatarUrl,
        // }
    }

    async banUser(userId: number, period: number): Promise<{ message: string }> {

        const user = await this.usersRepository.findOne(userId);
        const daysInMilliseconds = period * 86400000
        const banTime = (daysInMilliseconds / 3600000) / 24;

        

        user.banEndDate = new Date(Date.now() + daysInMilliseconds);
        // console.log(new Date(Date.now() + daysInMilliseconds).toLocaleString())
        await this.usersRepository.save(user);

        await this.booksRepository
            .createQueryBuilder()
            .update(Book)
            .set({ borrowedBy: null, status: BookStatus.FREE })
            .where('borrowedById = :id', { id: userId })
            .execute();

        return {
            message: `User with id: ${userId} has been banned for ${banTime} ${(banTime <= 1) ? 'day' : 'days'}!`
            // message: `User with id: ${userId} has been banned for ${banTime} ${(banTime <= 1) ? 'hour' : 'hours'}!`
        }
        
    }

    async deleteUser(userId: number): Promise<{ message: string }> {

        const userToDelete = await this.usersRepository.findOne(userId, {
            where: {
                isDeleted: false,
            }
        });
        if (!userToDelete) {
            throw new LibraryBusinessError(`User with id ${userId} not found!`, ErrorType.UserNotFound);
        }
        userToDelete.isDeleted = true;
        await this.usersRepository.save(userToDelete);
        await this.reviewsRepository
            .createQueryBuilder()
            .update(Review)
            .set({ isDeleted: true })
            .where('authorId = :id', { id: userId })
            .execute();

        await this.booksRepository
            .createQueryBuilder()
            .update(Book)
            .set({ borrowedBy: null, status: BookStatus.FREE })
            .where('borrowedById = :id', { id: userId })
            .execute();

            
        return { message: `User with id : ${userId} has been deleted!` }
    }
}
