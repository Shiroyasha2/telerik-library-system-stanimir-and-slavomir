import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { Review } from 'src/models/reviews.entity';
import { Book } from 'src/models/books.entity';
import { CreateReviewDTO } from 'src/dtos/reviewDtos/create-review.dto';
import { LibraryBusinessError } from 'src/error-handling/library-business.error';
import { ErrorType } from 'src/models/enums/error-type';
import { User } from 'src/models/users.entity';
import { ReturnReviewDTO } from 'src/dtos/reviewDtos/return-review.dto';
import { TransformService } from './transform.service';

@Injectable()
export class ReviewsService {
     private reviewsRepository: Repository<Review>;
     private bookRepository: Repository<Book>;
     private usersRepository: Repository<User>;
     constructor(
          @InjectRepository(Review) reviewsRepository: Repository<Review>,
          @InjectRepository(Book) bookRepository: Repository<Book>,
          @InjectRepository(User) usersRepository: Repository<User>,
          private readonly reviewTransformer: TransformService
     ) {
          this.reviewsRepository = reviewsRepository;
          this.bookRepository = bookRepository;
          this.usersRepository = usersRepository;
     }
     async getAll(): Promise<ReturnReviewDTO[]> {
          const result = await this.reviewsRepository.find({
               where: {
                    isDeleted : false,
               },
               relations: ['book'],
          });
          return result.map(review => this.reviewTransformer.toReturnReviewDTO(review));
     }

     async getReviewsByBookId(seachedBookId: number): Promise<ReturnReviewDTO[]> {
          const bookReviews = await this.reviewsRepository.find({
               relations: ['book','author','votes'],
          });
          
          const bookIdFromRepo = await this.bookRepository.findOne({
               where : {
                    id: seachedBookId,
               },
               select: ['id'],
          });
          if (!bookIdFromRepo) {
               throw new LibraryBusinessError(`Book with id ${seachedBookId} does not exist!`, ErrorType.BookNotFound);
          }
          
          
          const bookReviewsToReturn = bookReviews.filter(bookReview => bookReview.book && bookReview.book.id === seachedBookId && bookReview.isDeleted === false) ;

          return bookReviewsToReturn.map(review => this.reviewTransformer.toReturnReviewDTO(review));
     }

     // @Post('books/:id/reviews') 
     async createReview(newReview: CreateReviewDTO, seachedBookId: number, request: any): Promise<ReturnReviewDTO> {
          const author = await this.usersRepository.findOne({
               where: {
                    id: request.user.id
               }
          });
          const bookToReview = await this.bookRepository.findOne({
               where: {
                    id: seachedBookId,
               },
               relations: ['review'],
          });
          const bookIdFromRepo = await this.bookRepository.findOne({
               where : {
                    id: seachedBookId,
               },
               select: ['id'],
          });
          if (!bookIdFromRepo) {
               throw new LibraryBusinessError(`Book with id ${seachedBookId} does not exist!`, ErrorType.BookNotFound);
          }
          const reviewToCreate = new Review();
          reviewToCreate.content = newReview.content;
          reviewToCreate.author = author;
          reviewToCreate.book = bookIdFromRepo;
          const reviewToSave = await this.reviewsRepository.save(reviewToCreate);
          bookToReview.review.push(reviewToSave);
          const savedItem = await this.bookRepository.save(bookToReview);

          return this.reviewTransformer.toReturnReviewDTO(reviewToSave);
     }

     async deleteReview(seachedBookId: number, reviewId: number, request: any): Promise<ReturnReviewDTO> {
          const author = await this.usersRepository.findOne({
               where: {
                    id: request.user.id
               }
          });
          const bookReviewHolder = await this.bookRepository.findOne({
               where: {
                    id: seachedBookId,
               },
               relations: ['review'],
          });
          if (!bookReviewHolder) {
               throw new LibraryBusinessError(`Book with id ${seachedBookId} does not exist!`, ErrorType.BookNotFound);
          }
          const selectedReview = bookReviewHolder.review.find(review => review.id === reviewId);
          if (!selectedReview) {
               throw new LibraryBusinessError(`Review with id ${reviewId} does not exist!`, ErrorType.ReviewNotFound);
          }
          const selectedReviewAuthor = await this.reviewsRepository.findOne({
               where: {
                    id: selectedReview.id,
               },
               relations:['author','book']
          });
          
          if (selectedReview.isDeleted === true) {
               throw new LibraryBusinessError(`Review with id ${reviewId} is already deleted!`, ErrorType.ReviewAlreadyDeleted);  
          }
          
          if ( !(selectedReviewAuthor.author.id === author.id )) {
               throw new LibraryBusinessError(`You can not delete review posted by another user`, ErrorType.ForeignAuthor)
          }
          
          selectedReview.isDeleted = true;
          
          await this.reviewsRepository.save(selectedReview);
          return this.reviewTransformer.toReturnReviewDTO(selectedReviewAuthor);
     }

     async updateReview(seachedBookId: number, reviewId: number, newReviewData: Partial<Review>, request: any): Promise<ReturnReviewDTO> {
          const bookReviewHolder = await this.bookRepository.findOne({
               where: {
                    id: seachedBookId,
               },
               relations: ['review'],
          });
          if (!bookReviewHolder) {
               throw new LibraryBusinessError(`Book with id ${seachedBookId} does not exist!`, ErrorType.BookNotFound);
          }
          const selectedReview = bookReviewHolder.review.find(review => review.id === reviewId);
          if (!selectedReview) {
               throw new LibraryBusinessError(`Review with id ${reviewId} does not exist!`, ErrorType.ReviewNotFound);
          }
          const author = await this.usersRepository.findOne({
               where: {
                    id: request.user.id
               }
          });
          const selectedReviewAuthor = await this.reviewsRepository.findOne({
               where: {
                    id: selectedReview.id,
               },
               relations:['author', 'book']
          });
          if ( !(selectedReviewAuthor.author.id === author.id )) {
               throw new LibraryBusinessError(`You can not edit review posted by another user`, ErrorType.ForeignAuthor)
          }
  
          selectedReview.content = newReviewData.content;
          await this.reviewsRepository.save(selectedReview);
          return this.reviewTransformer.toReturnReviewDTO(selectedReviewAuthor);
     }

}
