import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Book } from 'src/models/books.entity';
import { Repository, Not } from 'typeorm';
import { BookStatus } from 'src/models/enums/book-status';
import { User } from 'src/models/users.entity';
import { Rate } from 'src/models/rate.entity';
import { BookRating } from 'src/models/enums/book-rating';
import { LibraryBusinessError } from 'src/error-handling/library-business.error';
import { ErrorType } from 'src/models/enums/error-type';
import { TransformService } from './transform.service';
import { ReturnBookDTO } from 'src/dtos/bookDtos/return-book.dto';
import { ReturnBookWithBorrowedDTO } from 'src/dtos/bookDtos/book-with-borrowed.dto';
import { ReturnBookWithRatingFromUserDTO } from 'src/dtos/bookDtos/book-with-rating-from-user.dto';
import { ReturnBookWithRatingDTO } from 'src/dtos/bookDtos/book-with-rating.dto';

@Injectable()
export class BooksService {
    constructor(
        @InjectRepository(Book) private readonly bookRepository: Repository<Book>,
        @InjectRepository(User) private readonly userRepository: Repository<User>,
        @InjectRepository(Rate) private readonly rateRepository: Repository<Rate>,

        private readonly transformService: TransformService


    ) { }

    async getAll(): Promise<ReturnBookDTO[]> {
        
        const books = await this.bookRepository.find({
            where: {
                status: Not(3)
            },
            relations: ['borrowedBy']
        });
        return books.map(book => this.transformService.toBookDTO(book));
    }

    async getBookById(bookId: number): Promise<ReturnBookDTO> {
        const book = await this.bookRepository.findOne({
            where: { id: bookId },
            relations: ['review', 'borrowedBy']
        });


        this.checkIfBookExists(book);

        return this.transformService.toBookDTO(book);
    }

    async borrowBook(bookId: number, userId: number): Promise<ReturnBookWithBorrowedDTO> {
        const book = await this.bookRepository.findOne({
            where: { id: bookId },
            relations: ['borrowHistory']
        });

        const user = await this.userRepository.findOne(userId);

        this.checkIfBookExists(book);
        console.log(book.status)
        console.log(book.borrowedBy)

        if (book.status === BookStatus.BORROWED) {
            throw new LibraryBusinessError('This book is borrowed!', ErrorType.BookNotFree);
        }

        book.borrowedBy = user;
        book.status = BookStatus.BORROWED;
        book.borrowHistory.push(user);

        await this.bookRepository.save(book);

        return this.transformService.toBookWithBorrowed(book);

    }

    async returnBook(bookId: number, userId: number): Promise<ReturnBookDTO> {
        const book = await this.bookRepository.findOne({
            where: { id: bookId },
            relations: ['borrowedBy']
        });

        this.checkIfBookExists(book);

        if (book.status === BookStatus.FREE) {
            throw new LibraryBusinessError('This book is not borrowed!', ErrorType.BookNotBorrowed);
        }

        if (book.borrowedBy.id !== userId) { 
            throw new LibraryBusinessError('This book is not borrowed by this user!', ErrorType.BookNotBorrowedByUser);
        }

        book.borrowedBy = null;
        book.status = BookStatus.FREE;
        await this.bookRepository.update(bookId, book);
        return this.transformService.toBookDTO(book);
    }

    async rateBook(bookId: number, rating: number, userId: number): Promise<ReturnBookWithRatingDTO> {
        const book = await this.bookRepository.findOne({
            where: { id: bookId },
            relations: ['borrowHistory', 'review', 'review.author', 'borrowedBy']
        });

        const user = await this.userRepository.findOne({
            where: { id: userId },
        });

        const rates = await this.rateRepository.find({
            where: {
                book: bookId
            },
            relations: ['user']
        })
        this.checkIfBookExists(book);
        this.checkIfUserCanRate(book, userId);
        this.CheckIfValidRating(rating);

        const ratingFromUser = rates.find(rate => rate.user.id === user.id)
        if (ratingFromUser) {
            ratingFromUser.rating = rating;

            await this.rateRepository.save(ratingFromUser);
        } else {
            const rate = new Rate();
            rate.rating = rating;
            rate.book = book;
            rate.user = user;

            await this.rateRepository.save(rate);
        }

        const ratingSum = rates.reduce((acc, rate) => acc += rate.rating, 0);
        const avgRating = ratingSum / rates.length;

        book.rating = avgRating;
        await this.bookRepository.save(book);

        return this.transformService.toBookWithRating(book)

    }

    async getBookRateByUser(userId: number, bookId: number): Promise<ReturnBookWithRatingFromUserDTO> {
        const book = await this.bookRepository.findOne({
            where: { id: bookId },
        });

        const rates = await this.rateRepository.find({
            where: {
                book: bookId
            },
            relations: ['user']
        })
        const ratingFromUser = rates.find(rate => rate.user.id === userId)
        if(ratingFromUser === undefined) {
            throw new BadRequestException('This user has not rated the book yet!')
        }

        return this.transformService.toBookWithRatingFromUser(book, userId, ratingFromUser.rating);
    }

    private checkIfUserCanRate(book: Book, userId: number) {
        const hasBorrowed = book.borrowHistory.some(user => user.id === userId);
        if (!hasBorrowed) {
            throw new LibraryBusinessError('The user hasn\'t borrowed this book!', ErrorType.NotBorrowed);
        }

        const hasNotReturnedBook = Boolean(book.borrowedBy?.id === userId);
        if (hasNotReturnedBook) {
            throw new LibraryBusinessError('The user needs to return the book in order to rate it!', ErrorType.NotReturned);
        }
        const hasReviewedTheBook = book.review.some(rev => rev.author.id === userId);
        if (!hasReviewedTheBook) {
            throw new LibraryBusinessError('The user needs to write a review in order to rate the book!', ErrorType.NoReview);
        }

    }

    private checkIfBookExists(book: Book) {
        if (book === undefined || book.status === BookStatus.UNLISTED) {
            throw new LibraryBusinessError('Book not found!', ErrorType.BookNotFound);
        }
    }

    private CheckIfValidRating(rating: number) {
        if (!Object.values(BookRating).includes(rating)) {
            throw new BadRequestException('Invalid rating!');
        }
    }

}
