import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { BooksService } from './books.service';
import { ReviewsService } from './reviews.service';
import { PassportModule } from '@nestjs/passport'
import { JwtModule } from '@nestjs/jwt'
import { jwtConstants } from 'src/constant/secret';
import { AuthService } from './auth.service';
import { JwtStrategy } from './strategy/jwt-strategy';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/models/users.entity';
import { Book } from 'src/models/books.entity';
import { Review } from 'src/models/reviews.entity';
import { Rate } from 'src/models/rate.entity';
import { Vote } from 'src/models/vote.entity';
import { VoteService } from './vote.service';
import { Token } from 'src/models/token.entity';
import { TransformService } from './transform.service';

@Module({
    imports: [
      TypeOrmModule.forFeature([User, Book, Review, Rate, Vote, Token]), 
        PassportModule,
        JwtModule.register({
          secret: jwtConstants.secret,
          signOptions: {
            expiresIn: '7d',
          }
        }),
      ],
    providers: [UsersService, BooksService, ReviewsService,VoteService, AuthService, JwtStrategy, TransformService],
    exports: [UsersService, BooksService, ReviewsService, VoteService, AuthService,]})
export class ServicesModule {}
