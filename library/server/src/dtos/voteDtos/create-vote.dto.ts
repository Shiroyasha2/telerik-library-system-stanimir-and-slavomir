import { ReviewReaction } from "src/models/enums/reaction";
import { IsEnum } from "class-validator";

export class CreateVoteDTO {
    id: number;
    @IsEnum(ReviewReaction)
    vote: ReviewReaction;
}