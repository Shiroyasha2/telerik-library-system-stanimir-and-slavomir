import { UserRole } from "src/models/enums/user-role";

export class ReturnFullUserDTO {
    id: number;
    username: string;
    avatarUrl: string;
    isDeleted: boolean;
    banEndDate: Date;
    role: UserRole;
}