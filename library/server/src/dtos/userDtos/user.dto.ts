export class ReturnUserDTO {
    id: number;
    username: string;
    avatarUrl: string;
}