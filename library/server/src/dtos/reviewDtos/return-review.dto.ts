import { Vote } from 'src/models/vote.entity';
import { User } from '../../models/users.entity';

export class ReturnReviewDTO {
    content: string; 
    forBook: string;
    id: number;
    author: User;
    votes: Vote[];
}