import { Length } from 'class-validator';
export class CreateReviewDTO {
    
    @Length(4, 300)
    content: string;
    
    id: number;
}