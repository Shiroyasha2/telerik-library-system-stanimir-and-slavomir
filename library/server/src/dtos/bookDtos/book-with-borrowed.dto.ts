import { ReturnUserDTO } from "../userDtos/user.dto";

export class ReturnBookWithBorrowedDTO {
    title: string;
    author: string;
    borrowedBy: ReturnUserDTO;
}