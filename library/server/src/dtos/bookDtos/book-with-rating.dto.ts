import { User } from "src/models/users.entity";
import { ReturnUserDTO } from "../userDtos/user.dto";

export class ReturnBookWithRatingDTO {
    id: number;
    title: string;
    author: string;
    rating: number;
    borrowedBy: ReturnUserDTO;
}