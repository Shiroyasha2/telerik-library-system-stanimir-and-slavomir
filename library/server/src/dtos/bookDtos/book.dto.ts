import { BookRating } from "src/models/enums/book-rating";
import { Review } from "src/models/reviews.entity";

export class BookDTO {
    id: number;
    title: string;
    author: string;
    isBorrowed: boolean;
    rating: BookRating;
    review: Review[]
}