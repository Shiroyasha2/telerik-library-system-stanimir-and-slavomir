export class ReturnBookWithRatingFromUserDTO {
    userId: number
    BookId: number;
    rating: number;
}