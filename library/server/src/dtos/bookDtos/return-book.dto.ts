import { User } from "src/models/users.entity";
import { ReturnUserDTO } from "../userDtos/user.dto";

export class ReturnBookDTO {
    id: number;
    title: string;
    author: string;
    rating: number;
    borrowedBy: ReturnUserDTO;
    coverUrl: string;
}