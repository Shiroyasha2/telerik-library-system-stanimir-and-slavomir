import React, { useState, useEffect } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import './Carousel.css'
import { BASE_URL } from '../../common/constants';
import StarRating from '../InteractiveIcons/StarRating/StarRating';
import NotFound from '../Pages/NotFound/NotFound';
import { withRouter } from 'react-router-dom';

const CenterMode = (props) => {
    const [books, setBooks] = useState([]);
    const [error, setError] = useState(null);
    const history = props.history;


    const settings = {
        className: "center",
        centerMode: true,
        infinite: true,
        centerPadding: "60px",
        slidesToShow: 3,
        speed: 250
    };

    useEffect(() => {
        fetch(`${BASE_URL}/books`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
            },
        })
            .then(request => request.json())
            .then(data => {
                if (data.error) {
                    throw new Error(data.message);
                }
                setBooks(data);
            })
            .catch(error => setError(error.message));
    }, []);

    if (error) {
        return (
            <NotFound message={error.message} />
        );
    };

    const displayBook = (book) => {
        return (
            <div className="box" key={book.id} onClick={() => history.push("/boook/" + book.id)}>
                <img className="cover" src={`${BASE_URL}/covers/${book.coverUrl}`} alt="book cover"></img>
                <div className="info-container">
                    <div className="info-data">
                        <span>{book.title}</span> <br />
                        <StarRating rating={book.rating} />
                    </div>
                </div>
            </div>
        );
    };

    return (
        <div>
            <Slider {...settings}>
                {books.map((book) => displayBook(book))}
            </Slider>
        </div>
    );
};

export default withRouter(CenterMode);