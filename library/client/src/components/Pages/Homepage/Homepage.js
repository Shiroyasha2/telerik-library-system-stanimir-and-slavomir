import React from 'react';
import './Homepage.css';
import CenterMode from '../../Carousel/Carousel';

const Homepage = () => {

    return (
        <div className="Homepage">
            <p className="p1">This month's</p>
            <p className="p2">RECOMMENDED BOOKS</p>
            <CenterMode />
        </div>
    );
};
export default Homepage;