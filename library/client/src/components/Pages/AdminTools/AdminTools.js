import React, { useEffect, useState } from 'react';
import './AdminTools.css';
import { BASE_URL } from '../../../common/constants';
import NotFound from '../NotFound/NotFound';

const AdminTools = () => {
    const [users, setUsers] = useState([]);
    const [error, setError] = useState(null);
    const [banPeriod, setBanPeriod] = useState(null)

    useEffect(() => {
        fetch(`${BASE_URL}/users`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
            },
        })
            .then(request => request.json())
            .then(data => {
                if (data.error) {
                    throw new Error(data.message);
                }
                const users = data.filter(user => user.role === 1)
                setUsers(users);
            })
            .catch(error => setError(error.message));
    }, []);

    const deleteUser = (userId) => {
        fetch(`${BASE_URL}/users/${userId}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token') || ''}`
            },
        })
            .then(request => request.json())
            .then((data) => {
                if (data.error) {
                    throw new Error(data.message);
                }
            })
            .catch(error => setError(error.message));
    }

    const banUser = (userId) => {
        fetch(`${BASE_URL}/users/${userId}/ban`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token') || ''}`
            },
            body: JSON.stringify({
                period: Number(banPeriod),
            }),
        })
            .then(request => request.json())
            .then((data) => {
                if (data.error) {
                    throw new Error(data.message);
                }
            })
            .catch(error => setError(error.message));
    };

    const UserStatus = (userId) => {
        const user = users.filter(user => user.id === userId)

        if (user[0].isDeleted === true) {
            return (
                <div className="user-status" style={{ color: "red" }}>/Deleted/</div>
            )

        }

        if (user[0].banEndDate !== null) {
            return (
                <div className="user-status" style={{ color: "white" }}>{`/Banned until: ${user[0].banEndDate.toLocaleString().split('T')[0]} ${user[0].banEndDate.toLocaleString().split('T')[1].slice(0, 8)} /`}</div>
            )
        }

        return (
            <div className="user-status" style={{ color: "lightgreen" }}>/Active/</div>
        );
    };

    if (error) {
        return (
            <NotFound message={error} />
        );
    };

    return (
        <div className="admin-tools">
            <h2 style={{ textAlign: "center", color: "white" }}>Admin Tools</h2>
            <div className="users-container">
                {(users) &&
                    users.map(user => {
                        return (
                            <div className="user" key={user.id}>
                                <span style={{ color: "white" }} key={user.id}>{user.username}</span>
                                {UserStatus(user.id)}
                                <div className="tools">
                                    <div className="tool-btns">
                                        <span id="delete-user-btn" onClick={() => deleteUser(user.id)}>Delete</span>
                                        <span id="ban-user-btn" onClick={() => banUser(user.id)}>Ban</span>
                                    </div>
                                    <select id="ban-period" defaultValue="1" onChange={(e) => setBanPeriod(e.target.value)}>
                                        <option value="1" >1 day</option>
                                        <option value="3">3 days</option>
                                        <option value="7">7 days</option>
                                        <option value="14">14 days</option>
                                    </select>
                                </div>

                            </div>
                        )
                    })
                }
            </div>
        </div>
    );
};

export default AdminTools;