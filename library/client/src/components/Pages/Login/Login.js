import React, { useState, useContext } from 'react';
import './Login.css';
import { BASE_URL } from '../../../common/constants';
import AuthContext from '../../../providers/AuthContext';
import jwtDecode from 'jwt-decode';

const Login = (props) => {
    const history = props.history;
    const { setLoginState } = useContext(AuthContext);
    const [errorMsg, setErrorMsg] = useState(null)

    const [user, setUserObject] = useState({
        username: {
            value: '',
            touched: false,
            valid: true,
        },
        password: {
            value: '',
            touched: false,
            valid: true,
        },
    });

    const updateUser = (prop, value) => setUserObject({
        ...user,
        [prop]: {
            value,
            touched: true,
            valid: userValidators[prop].reduce((isValid, validatorFn) => isValid && (typeof validatorFn(value) !== 'string'), true),
        }
    });

    const validateForm = () => !Object
        .keys(user)
        .reduce((isValid, prop) => isValid && user[prop].valid && user[prop].touched, true);

    const userValidators = {
        username: [
            value => value?.length >= 4 || `Username should be at least 4 letters.`,
            value => value?.length <= 10 || `Username should be no more than 10 letters.`,
        ],
        password: [
            value => value?.length >= 4 || `Password should be at least 4 letters.`,
            value => value?.length <= 10 || `Password should be no more than 10 letters.`,
            value => /[a-zA-Z]/.test(value) || `Password should contain at least one letter.`,
            value => /[0-9]/.test(value) || `Password should contain at least one number.`,
        ],
    };

    const getValidationErrors = (prop) => {
        return userValidators[prop]
            .map(validatorFn => validatorFn(user[prop].value)) // [string, true, true, string]
            .filter(value => typeof value === 'string');
    };

    const renderValidationError = prop => user[prop].touched && !user[prop].valid
        ? getValidationErrors(prop).map((error, index) => <p className="error" key={index}>{error}</p>)
        : null;

    const getClassNames = (prop) => {
        let classes = '';
        if (user[prop].touched) {
            classes += 'touched '
        }
        if (user[prop].valid) {
            classes += 'valid ';
        } else {
            classes += 'invalid ';
        }

        return classes;
    };

    const login = () => {
        fetch(`${BASE_URL}/session `, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ username: user.username.value, password: user.password.value }),
        })
            .then(r => r.json())
            .then(result => {
                
                if (result.error) {
                    throw new Error(result.message);
                }
                const payload = jwtDecode(result.token);


                if (payload.banEndDate > new Date().toISOString()) {
                    throw new Error(`You are banned until ${payload.banEndDate.toLocaleString().split('T')[0]} ${payload.banEndDate.toLocaleString().split('T')[1].slice(0, 8)}`)
                }

                setLoginState({ isLoggedIn: true, user: payload });
                localStorage.setItem('token', result.token);
                history.push('/homepage');

            })
            .catch(error => setErrorMsg(error.message));
    };




    return (
        <div className="LoginForm">
            <h1 className="header">Login</h1>
            <label className="input-username-label" htmlFor="input-username">Username: </label>
            <input className={"input-username " + getClassNames('username')} type="text" value={user.username.value} onChange={(e) => updateUser('username', e.target.value)} /><br />
            {renderValidationError('username')}<br /><br />
            <label className="input-password-label" htmlFor="input-password">Password:</label>
            <input className={"input-password " + getClassNames('password')} type="password" value={user.password.value} onChange={(e) => updateUser('password', e.target.value)} /><br /><br />
            {renderValidationError('password')}<br /><br />
            {errorMsg && <p className="error" >{errorMsg}</p>}
            <button className="login-button" onClick={login} disabled={validateForm()}>Login</button>
        </div>
    )
};
export default Login;