import React, { useEffect, useState } from 'react';
import './UserProfile.css';
import { BASE_URL } from '../../../common/constants';
import { extractUser } from '../../../providers/AuthContext';
import SingleBook from '../../Resources/Books/SingleBook/SingleBook';
import NotFound from '../NotFound/NotFound';

const UserProfile = (props) => {

    const [books, setBooks] = useState([]);
    const [error, setError] = useState(null);
    const [fakeRefresh, setRefresh] = useState(0)
    const user = extractUser();
    useEffect(() => {
        fetch(`${BASE_URL}/books`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
            },
        })
            .then(request => request.json())
            .then(data => {
                if (data.error) {
                    throw new Error(data.message);
                }
                setBooks(data);
            })
            .catch(error => setError(error.message));

    }, [fakeRefresh]);

    const tempBooks = books.filter(book => {
        if (book.borrowedBy) {
            return user.id === book.borrowedBy.id
        } else {
            return undefined;
        }
    });

    if (error) {
        return (
            <NotFound message={error} />
        );
    };

    return (
        <div className="user-info-container">
            <img className="user-avatar" src={`${BASE_URL}/avatars/${user?.avatarUrl}`} alt="user avatar"></img>
            <h2 className="user-username">{user.username}</h2>
            <>
                <h2 style={{ textAlign: "center" }}>My Borrowed Books</h2>
                <h3 style={{ textAlign: "center" }}>{tempBooks.length > 0 ? `Total borrowed books: ${tempBooks.length}` : 'You haven\'t borrowed any books yet!'}</h3>
                <div className="borrowed-books" >
                    {tempBooks &&
                        tempBooks.length > 0 &&
                        tempBooks.slice().map((book) => <SingleBook key={book.id} inProfile="in-profile" book={book} setRefresh={setRefresh} fakeRefresh={fakeRefresh} />)}
                </div>
            </>
        </div>
    );
};

export default UserProfile;