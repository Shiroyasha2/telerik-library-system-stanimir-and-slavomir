import React from 'react';

const NotFound = (props) => {
    return (
        <div>
            {props.message
                ? <h1 style={{ color: "red", textAlign: "center" }} >{props.message}</h1>
                : <h1 style={{ color: "red", textAlign: "center" }} >404 Not Found</h1>
            }
        </div>
    );
};

export default NotFound;