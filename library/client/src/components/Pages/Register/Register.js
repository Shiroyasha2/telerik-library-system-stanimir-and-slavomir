import React, { useState } from 'react';
import { BASE_URL } from '../../../common/constants'
import './Register.css'

const Register = (props) => {
    const history = props.history;
    const [files, setFiles] = useState([])
    const [user, setUserObject] = useState({
        username: {
            value: '',
            touched: false,
            valid: true,
        },
        password: {
            value: '',
            touched: false,
            valid: true,
        },
        confirm: {
            value: '',
            touched: false,
            valid: true,
        }
    });

    const updateUser = (prop, value) => setUserObject({
        ...user,
        [prop]: {
            value,
            touched: true,
            valid: userValidators[prop].reduce((isValid, validatorFn) => isValid && (typeof validatorFn(value) !== 'string'), true),
        }
    });

    const validateForm = () => !Object
        .keys(user)
        .reduce((isValid, prop) => isValid && user[prop].valid && user[prop].touched, true);

    const userValidators = {
        username: [
            value => value?.length >= 4 || `Username should be at least 4 letters.`,
            value => value?.length <= 10 || `Username should be no more than 10 letters.`,
        ],
        password: [
            value => value?.length >= 4 || `Password should be at least 4 letters.`,
            value => value?.length <= 10 || `Password should be no more than 10 letters.`,
            value => /[a-zA-Z]/.test(value) || `Password should contain at least one letter.`,
            value => /[0-9]/.test(value) || `Password should contain at least one number.`,
        ],
        confirm: [
            value => value && value === user.password.value || `Confirm should match the password.`,
        ],

    };

    const getValidationErrors = (prop) => {
        return userValidators[prop]
            .map(validatorFn => validatorFn(user[prop].value))
            .filter(value => typeof value === 'string');
    };

    const renderValidationError = prop => user[prop].touched && !user[prop].valid
        ? getValidationErrors(prop).map((error, index) => <p className="error" key={index}>{error}</p>)
        : null;


    const getClassNames = (prop) => {
        let classes = '';
        if (user[prop].touched) {
            classes += 'touched '
        }
        if (user[prop].valid) {
            classes += 'valid ';
        } else {
            classes += 'invalid ';
        }

        return classes;
    };

    const uploadAvatar = (id) => {
        const formData = new FormData();
        if (!files.length) {
            return;
        }
        formData.append('files', files[0])
        fetch(`${BASE_URL}/users/${id}/avatar`, {
            method: 'POST',
            body: formData,
        })
            .then(response => response.json())
            .then(console.log)
            .catch(console.warn)
            .finally(() => history.push('/login'))
    }

    const register = () => {
        fetch(`${BASE_URL}/users `, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ username: user.username.value, password: user.password.value }),
        })
            .then(r => r.json())
            .then(result => {
                if (result.error) {
                    return alert(result.message);
                }
                uploadAvatar(result.id)
            })
            .catch(alert);
    }


    return (
        <div className="RegisterForm">
            <h1 className="header">{'Register'}</h1>
            <label className="input-username-label" htmlFor="input-username">Username: </label>
            <input className={"input-username " + getClassNames('username')} type="text" value={user.username.value} onChange={(e) => updateUser('username', e.target.value)} /><br />
            {renderValidationError('username')}<br /><br />

            <label className="input-password-label" htmlFor="input-password">Password:</label>
            <input className={"input-password " + getClassNames('password')} type="password" value={user.password.value} onChange={(e) => updateUser('password', e.target.value)} /><br />
            {renderValidationError('password')}<br /><br />

            <label className="input-password-confirm-label" htmlFor="input-password">Confirm:</label>
            <input className={"input-password-confirm " + getClassNames('confirm')} type="password" value={user.confirm.value} onChange={(e) => updateUser('confirm', e.target.value)} /><br /><br />
            {renderValidationError('confirm')}<br /><br />

            <label className="file-path-label" htmlFor="file-path">Select avatar:</label>
            <input className="file-path" type="file" onChange={(e) => setFiles(Array.from(e.target.files))}></input><br /><br />

            <button className="register-button" onClick={register} disabled={validateForm()}>Register</button>
        </div>
    );
};

export default Register;