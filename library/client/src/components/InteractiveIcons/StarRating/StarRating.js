import React from 'react';
import './StarRating.css';

const StarRating = ({ rating }) => {
    let localRating = Math.round(rating);
    let starContainer = [0, 0, 0, 0, 0];
    starContainer = starContainer.map((star) => {
        if (localRating > 0) {
            --localRating;
            return true;
        } else {
            return false;
        }
    })

    return (
        <>
            {starContainer.map((star, index) => {
                return (
                    (star > 0)
                        ? <span key={index} className="fa fa-star checked"></span>
                        : <span style={{ color: "silver" }} key={index} className="fa fa-star"></span>
                )
            })
            }
        </>
    );
};

export default StarRating;