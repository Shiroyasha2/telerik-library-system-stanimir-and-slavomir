import React, { useEffect, useState } from 'react';
import { BASE_URL } from '../../../../common/constants';
import './VoteReview.css';
import NotFound from '../../../Pages/NotFound/NotFound';

const VoteReview = (props) => {
    const [error, setError] = useState(null);
    const [userVotes, setUserVotes] = useState([]);
    const [fakeState, setFakeState] = useState(0);
    const loggedUserId = props.loggedUserId;
    const reviewId = props.review.id;
    let likesDisplayed = 0;
    let dislikesDisplayed = 0;

    useEffect(() => {
        fetch(`${BASE_URL}/reviews/${props.review.id}/votes`, {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
            },
        })
            .then(request => request.json())
            .then(data => {
                if (data.error) {
                    throw new Error(data.error);
                }
                setUserVotes(data);
            })
            .catch((error) => setError(error.message));
    }, [fakeState]);

    userVotes.slice().forEach(element => {
        if (element.vote === 2) {
            ++likesDisplayed;
        } else if (element.vote === 3) {
            ++dislikesDisplayed;
        }
    });

    const hasThisUserVotedThisReview = () => {
        const checkIfIsVoted = userVotes.some((currentVote) => loggedUserId === currentVote.user.id);
        return checkIfIsVoted;
    };

    const isLiked = () => {
        if (hasThisUserVotedThisReview()) {
            const findVote = userVotes.find(vote => vote.user.id === loggedUserId);
            if (findVote.vote === 2) {
                return 'votted';
            };
        };
        return '';
    };

    const isDisliked = () => {
        if (hasThisUserVotedThisReview()) {
            const findVote = userVotes.find(vote => vote.user.id === loggedUserId);
            if (findVote.vote === 3) {
                return 'votted';
            };
        };
        return '';
    };

    const likeHandle = () => {
        if (!hasThisUserVotedThisReview()) {
            setVote(reviewId, 2);
        } else {
            if (!isLiked()) {
                updateVote(reviewId, 2);
            };
        };
    };

    const dislikeHandle = () => {
        if (!hasThisUserVotedThisReview()) {
            setVote(reviewId, 3);
        } else {
            if (!isDisliked()) {
                updateVote(reviewId, 3);
            };
        };
    };

    const updateVote = (reviewId, status) => {
        fetch(`${BASE_URL}/reviews/${reviewId}/votes`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
            },
            body: JSON.stringify({
                "vote": status,
            }),
        })
            .then(request => request.json())
            .then(data => {
                if (data.error) {
                    throw new Error(data.error);
                }
            })
            .catch((error) => setError(error.message));
    };

    const setVote = (reviewId, status) => {
        (!hasThisUserVotedThisReview()) &&
            fetch(`${BASE_URL}/reviews/${reviewId}/votes`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
                },
                body: JSON.stringify({
                    "vote": status,
                }),
            })
                .then(request => request.json())
                .then(data => {
                    if (data.error) {
                        throw new Error(data.error);
                    }
                })
                .catch((error) => setError(error.message));
    };

    if (error) {
        return (
            <NotFound message={error} />
        );
    };
    
    return (
        <div className="voteInfo">
            <i
                onMouseDown={() => likeHandle()}
                className={`fa fa-thumbs-up ${isLiked()}`}
                onMouseUp={() => setFakeState(Math.random())}>
            </i>{likesDisplayed}
            <i
                onMouseDown={() => dislikeHandle()}
                className={`fa fa-thumbs-down ${isDisliked()}`}
                onMouseUp={() => setFakeState(Math.random())}>
            </i>{dislikesDisplayed}
        </div>
    );
};

export default VoteReview;