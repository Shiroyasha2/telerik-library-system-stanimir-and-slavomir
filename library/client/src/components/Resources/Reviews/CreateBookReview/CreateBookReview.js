import React, { useState } from 'react';
import { BASE_URL } from '../../../../common/constants';
import './CreateBookReview.css';
import NotFound from '../../../Pages/NotFound/NotFound';

const CreateBookReview = (props) => {
    const id = props.id;
    const [reviewContent, setReviewContent] = useState('');
    const [error, setError] = useState(null);

    const postReview = () => {
        fetch(`${BASE_URL}/books/${id}/reviews`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token') || ''}`
            },
            body: JSON.stringify({
                content: reviewContent,
            }),
        })
            .then(request => request.json())
            .then((data) => {
                if (data.error) {
                    throw new Error(data.message);
                }
                setReviewContent(data);
                props.setIsCreateReviewFormVisible(false);
                if (props.areReviewsVisible) {
                    props.setAreReviewsVisible(false);
                    props.setAreReviewsVisible(true);
                }
            })
            .catch(error => setError(error.message));
    };
    
    if (error) {
        return (
            <NotFound message={error.message} />
        );
    };

    return (
        <div><br />
            <div className="WriteReviewContainer">
                <span className="SingleReview">
                    <input id="inputField" type="text" onChange={(ev) => setReviewContent(ev.target.value)}></input>
                </span><br />
            </div>
            <button id="PostReviewButton" onClick={postReview}>Post</button><br />
        </div>
    );
};

export default CreateBookReview;