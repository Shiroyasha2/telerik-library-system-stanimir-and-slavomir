import React, { useState } from 'react';
import './SingleReview.css';
import { extractUser } from '../../../../providers/AuthContext';
import { BASE_URL } from '../../../../common/constants';
import NotFound from '../../../Pages/NotFound/NotFound';
import VoteReview from '../VoteReview/VoteReview';

const SingleReview = (props) => {
    const [isDeleteButtonVisible, setIsDeleteButtonVisible] = useState(false);
    const [editReviewContent, updateReviewContent] = useState(props.review.content);
    const [editReviewMode, setEditReviewMode] = useState(false);
    const [error, setError] = useState(null);
    const loggedUser = extractUser().username;
    const loggedUserId = extractUser().id;
    const deleteReview = () => props.deleteReview(props.review.id);

    const editReview = () => {
        fetch(`${BASE_URL}/books/${props.bookId}/reviews/${props.review.id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token') || ''}`
            },
            body: JSON.stringify({
                content: editReviewContent,
            }),
        })
            .then(request => request.json())
            .then((data) => {
                if (data.error) {
                    throw new Error(data.message);
                }
                let updatedReviews = [];
                updatedReviews = props.reviews.map((review) => {
                    if (review.id === props.review.id) {
                        return ({
                            ...review,
                            content: editReviewContent
                        });
                    } else {
                        return review;
                    }
                })
                props.setReviews(updatedReviews);
            })
            .catch(error => setError(error.message));
    };


    const canBeEditedByCurrentUser = () => {
        if (loggedUser === props.review.author.username) {
            return 'editable';
        } else {
            return undefined;
        };
    };

    if (error) {
        return (
            <NotFound message={error} />
        );
    };

    return (
        <div className="ReviewContainer"
            onMouseEnter={() => setIsDeleteButtonVisible(true)}
            onMouseLeave={() => setIsDeleteButtonVisible(false)}>
            <div className="SingleReview">
                <input
                    className={`ReviewContent ${canBeEditedByCurrentUser()}`}
                    disabled={!(canBeEditedByCurrentUser())}
                    onChange={(e) => updateReviewContent(e.target.value)}
                    onClick={setEditReviewMode}
                    type="text"
                    onBlur={(e) => e.target.value = ''}
                    placeholder={`"` + props.review.content + `"`}>
                </input><br />
                <span id="ReviewAuthor">{props.review.author.username}</span><br />
                <div>
                </div>
                <span style={{ float: "right" }}>
                    <VoteReview
                        loggedUser={loggedUser}
                        loggedUserId={loggedUserId}
                        review={props.review}
                        editReviewContent={editReviewContent}
                        updateReviewContent={updateReviewContent}
                    />
                </span>
                {isDeleteButtonVisible &&
                    (loggedUser === props.review.author.username) &&
                    <>
                        <button id="DeleteReviewButton" onClick={deleteReview}>Delete</button>
                        {editReviewMode && <button id="EditReviewButton" onClick={editReview}>Save</button>}
                    </>
                }
            </div>
        </div>
    );
};

export default SingleReview;