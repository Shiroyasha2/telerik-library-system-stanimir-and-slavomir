import React, { useState, useEffect } from 'react';
import { BASE_URL } from '../../../../common/constants';
import SingleReview from '../SingleReview/SingleReview';
import NotFound from '../../../Pages/NotFound/NotFound';
import './Reviews.css';

const Reviews = (props) => {
    const [reviews, setReviews] = useState(null);
    const [error, setError] = useState(null);
    const id = props.id

    useEffect(() => {
        fetch(`${BASE_URL}/books/${id}/reviews`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
            },
        })
            .then(request => request.json())
            .then(data => {
                if (data.error) {
                    throw new Error(data.error);
                }
                setReviews(data);
            })
            .catch((error) => setError(error.message));
    }, [id]);

    const deleteReview = (reviewId) => {
        fetch(`${BASE_URL}/books/${id}/reviews/${reviewId}`, {
            method: 'DELETE',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token') || ''}`
            },
        })
            .then(r => r.json())
            .then(data => {
                if (data.error) {
                    throw new Error(data.message);
                };
                const filteredReviews = reviews.slice().filter(revue => revue.id !== data.id);
                setReviews(filteredReviews);
            })
            .catch(error => setError(error.message));
    };

    if (error) {
        return (
            <NotFound message={error} />
        );
    };

    return (
        reviews &&
        <div className="Reviews">
            {reviews &&
                reviews.length > 0 &&
                reviews.slice().reverse().map((review) =>
                    <SingleReview key={review.id}
                        reviews={reviews.slice()}
                        review={review}
                        bookId={id}
                        deleteReview={deleteReview}
                        setReviews={(reviews) => setReviews(reviews)}
                        setAreReviewsVisible={props.setAreReviewsVisible}
                    />)}
        </div>
    );
};

export default Reviews;