import React, { useEffect, useState, useContext } from 'react';
import './Book.css';
import { BASE_URL } from '../../../../common/constants';
import StarRating from '../../../InteractiveIcons/StarRating/StarRating';
import Reviews from '../../Reviews/Reviews/Reviews';
import CreateBookReview from '../../Reviews/CreateBookReview/CreateBookReview';
import Borrow from '../Borrow/Borrow';
import RateBook from '../RateBook/RateBook';
import AuthContext from '../../../../providers/AuthContext';


const Book = (props) => {
    const [book, setBook] = useState(null);
    const [areReviewsVisible, setAreReviewsVisible] = useState(false);
    const [isCreateReviewFormVisible, setIsCreateReviewFormVisible] = useState(false);
    const [sumRating, setSumRating] = useState(null)
    const id = +props.location.pathname.split('').pop();
    const history = props.history;

    const { user } = useContext(AuthContext);

    if (!user) {
        history.push("/login")
    };

    useEffect(() => {
        fetch(`${BASE_URL}/books/${id}`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
            },
        })
            .then(request => request.json())
            .then(data => {
                if (data.error) {
                    return alert(`Alert form Book.js`, data.message);
                }
                setBook(data);
                setSumRating(data.rating)
            })
    }, [id]);

    const getReviews = () => {
        setAreReviewsVisible(!areReviewsVisible);
    };

    const createReview = () => {
        setIsCreateReviewFormVisible(!isCreateReviewFormVisible);
    };

    return (
        book &&
        <div className="Book-container">
            <div className="Book" >
                <img className="book-cover" src={`${BASE_URL}/covers/${book.coverUrl}`} alt="book cover"></img>
                <div className="book-info">
                    <p id="title">{book.title}</p>
                    <p id="author">Author: {book.author}</p>
                    <p id="rating">Rating: </p><StarRating rating={sumRating} /> <span className="rating-num" >{Number.parseFloat(sumRating).toFixed(1)}</span> <br />
                    <Borrow book={book} />
                </div>
                <div className="summary">
                    <p className="summary-text">"Duis pretium tristique libero. Morbi dolor mi, auctor in neque sed, vulputate accumsan nisi. In vel maximus enim, in bibendum urna. Cras suscipit dui in pretium eleifend. Duis volutpat enim orci, et luctus justo laoreet et. Phasellus sollicitudin eu nibh a sodales. Etiam viverra odio cursus tempor placerat.Duis pretium tristique libero. Morbi dolor mi, auctor in neque sed, vulputate accumsan nisi. In vel maximus enim, in bibendum urna. Cras suscipit dui in pretium eleifend. Duis volutpat enim orci, et luctus justo laoreet et. Phasellus sollicitudin eu nibh a sodales. Etiam viverra odio cursus tempor placerat."</p>
                </div>
                <div className="my-activity">
                    <RateBook book={book} setSumRating={setSumRating} />
                    <span className="bookOptionsButton" onClick={() => getReviews()} >Reviews</span>
                    <span className="bookOptionsButton" onClick={() => createReview()}>Write review</span>
                    {isCreateReviewFormVisible &&
                        <CreateBookReview
                            id={id}
                            setIsCreateReviewFormVisible={setIsCreateReviewFormVisible}
                            areReviewsVisible={areReviewsVisible}
                            setAreReviewsVisible={setAreReviewsVisible}
                        />}
                    {areReviewsVisible && <Reviews id={id} setAreReviewsVisible={setAreReviewsVisible} />}
                </div>
            </div>
        </div>


    );
};

export default Book;