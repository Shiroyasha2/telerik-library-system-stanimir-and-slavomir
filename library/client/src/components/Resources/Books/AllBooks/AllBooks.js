import React, { useEffect, useState } from 'react';
import { BASE_URL } from '../../../../common/constants';
import SingleBook from '../SingleBook/SingleBook';
import './AllBooks.css';
import NotFound from '../../../Pages/NotFound/NotFound';

const AllBooks = () => {
    const [books, setBooks] = useState([]);
    const [error, setError] = useState(null);
    useEffect(() => {
        fetch(`${BASE_URL}/books`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
            },
        })
            .then(request => request.json())
            .then(data => {
                if (data.error) {
                    throw new Error(data.message);
                }
                setBooks(data);
            })
            .catch(error => setError(error.message));
    }, []);

    if (error) {
        return (
            <NotFound message={error.message} />
        );
    };

    if (books.message) {
        return (
            <NotFound message={books.message} />
        )
    }
    return (
        <div className="AllBooks">
            {books &&
                books.length > 0 &&
                books.slice().map((book) => <SingleBook key={book.id} book={book} />)}
        </div>
    );
};

export default AllBooks;