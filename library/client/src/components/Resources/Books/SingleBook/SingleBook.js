import React from 'react';
import './SingleBook.css'
import { withRouter } from 'react-router-dom';
import StarRating from '../../../InteractiveIcons/StarRating/StarRating';
import Borrow from '../Borrow/Borrow';
import { BASE_URL } from '../../../../common/constants';

const SingleBook = ({ book, history, inProfile, setRefresh }) => {
    const details = () => {
        history.push("/boook/" + book.id);
    };
    
    return (
        <div className="SingleBook">
            <img className="book-Cover" src={`${BASE_URL}/covers/${book.coverUrl}`} alt="book cover"></img>
            <div className="info">
                <span className="text-title">Title: {book.title}</span> <br />
                <span className="text-author">Author: {book.author}</span> <br />
                <span className="text-rating">Rating: </span><StarRating rating={book.rating} /> <span style={{ color: "lightbrown" }}>{Number.parseFloat(book.rating).toFixed(1)}</span> <br />
            </div>
            <span className="details-button" onClick={() => details(book)}>Details</span>

            <Borrow className="borrowed-info" inProfile={inProfile} book={book} setRefresh={setRefresh} />
        </div>
    );
};

export default withRouter(SingleBook);