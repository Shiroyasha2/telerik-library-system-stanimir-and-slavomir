import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import './RateBook.css'
import { BASE_URL } from '../../../../common/constants';
import { extractUser } from '../../../../providers/AuthContext';

const RateBook = ({ book, setSumRating }) => {
    const star = <svg className="star" height="20" width="20" viewBox="0 -10 511.98685 511" xmlns="http://www.w3.org/2000/svg"><path d="M510.65234 185.90234c-3.35156-10.36718-12.54687-17.73047-23.42578-18.71093l-147.77344-13.41797L281.01954 17.0039C276.71093 6.98047 266.89843.49219 255.9961.49219s-20.71484 6.48828-25.02343 16.53515l-58.4336 136.7461L24.7422 167.1914c-10.85938 1.0039-20.03125 8.34375-23.40235 18.71093-3.37109 10.3672-.2578 21.73828 7.95704 28.90625l111.69921 97.96094-32.9375 145.08985c-2.41015 10.66796 1.73047 21.6953 10.58204 28.09375 4.7578 3.4375 10.32421 5.1875 15.9375 5.1875 4.83984 0 9.64062-1.3047 13.94921-3.88282l127.46875-76.1836 127.42188 76.1836c9.32422 5.60938 21.07812 5.09766 29.91016-1.30469 8.85546-6.41796 12.99218-17.44921 10.58203-28.09375l-32.9375-145.08984 111.69922-97.9414c8.21484-7.1875 11.35156-18.53907 7.98046-28.92579zm0 0" /></svg>
    const user = extractUser();
    const [rating, setRating] = useState(null);
    const [errorMsg, setErrorMsg] = useState(null);

    const [stars, setStars] = useState({
        star1: false,
        star2: false,
        star3: false,
        star4: false,
        star5: false,
    });

    const fillStars = (rating) => {
        const mappedStars = {}
        for (const prop in stars) {
            if (prop.slice(-1) <= rating) {
                mappedStars[prop] = true
            } else {
                mappedStars[prop] = false
            }
        }

        return mappedStars;
    };

    useEffect(() => {
        fetch(`${BASE_URL}/books/${book.id}/rate/${user.id}`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
            },
        })
            .then(response => response.json())
            .then(result => {
                if (result.error) {
                    throw new Error(result.message);
                }
                if (result.rating && result.rating !== Number(rating)) {
                    setStars(fillStars(result.rating))
                    setRating(result.rating);
                    setErrorMsg(null)
                }
            })
            .catch(error => console.log(error.message));
    })


    const updateStars = (prop) => {
        const keys = Object.keys(stars);
        const mappedStars = keys.reduce((stars, star, index) => {
            let starNum = Number(prop.slice(-1))

            if (index <= --starNum) {
                return {
                    ...stars,
                    [star]: true,
                };
            } else {
                return {
                    ...stars,
                    [star]: false,
                };
            }
        }, {});
        setStars(mappedStars);
    };

    const undoStars = () => {
        setStars(fillStars(rating))
    };

    const setClasses = (prop) => stars[prop] === true ? 'star-on' : 'star-off';

    const rate = (value) => {
        fetch(`${BASE_URL}/books/${book.id}/${value}`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token') || ''}`
            }
        })
            .then(response => response.json())
            .then(result => {
                if (result.message) {
                    throw new Error(result.description);
                }
                setStars(fillStars(value))
                setRating(value);
                setSumRating(result.rating)
                setErrorMsg(null)
            })
            .catch(result => setErrorMsg(result.message))
    };

    return (
        <div className="stars">
            <h5>My rating</h5>
            <span className={setClasses('star1')} onMouseEnter={(e) => updateStars('star1')} onMouseLeave={() => undoStars()} onClick={() => rate(1)}>{star}</span>
            <span className={setClasses('star2')} onMouseEnter={(e) => updateStars('star2')} onMouseLeave={() => undoStars()} onClick={() => rate(2)}>{star}</span>
            <span className={setClasses('star3')} onMouseEnter={(e) => updateStars('star3')} onMouseLeave={() => undoStars()} onClick={() => rate(3)}>{star}</span>
            <span className={setClasses('star4')} onMouseEnter={(e) => updateStars('star4')} onMouseLeave={() => undoStars()} onClick={() => rate(4)}>{star}</span>
            <span className={setClasses('star5')} onMouseEnter={(e) => updateStars('star5')} onMouseLeave={() => undoStars()} onClick={() => rate(5)}>{star}</span>
            {
                errorMsg && <div className="error-msg">{errorMsg}</div>}
        </div>

    )

}

export default withRouter(RateBook);