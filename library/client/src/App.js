import React, { useState } from 'react';
import './App.css';
import Homepage from './components/Pages/Homepage/Homepage';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import Register from './components/Pages/Register/Register';
import Login from './components/Pages/Login/Login';
import NotFound from './components/Pages/NotFound/NotFound';
import Header from './components/Base/Header/Header';
import AllBooks from './components/Resources/Books/AllBooks/AllBooks';
import Book from './components/Resources/Books/Book/Book';
import AuthContext, { getToken, extractUser } from './providers/AuthContext';
import UserProfile from './components/Pages/UserProfile/UserProfile';
import AdminTools from './components/Pages/AdminTools/AdminTools';
function App() {
  const [authValue, setAuthValue] = useState({
    isLoggedIn: !!extractUser(getToken()),
    user: extractUser(getToken())
  });

  return (
    <div className="App">
      <BrowserRouter>
        <AuthContext.Provider value={{ ...authValue, setLoginState: setAuthValue }}>
          <Header />
          <Switch>
            <Redirect exact from="/" to="/homepage" />
            <Route path="/homepage" component={Homepage} />
            <Route path="/register" component={Register} />
            <Route path="/login" component={Login} />
            <Route path="/admin_tools" component={AdminTools} />
            <Route path="/books/:id" component={Book} />
            <Route path="/books" component={AllBooks} />
            <Route path="/boook" component={Book} />
            <Route path="/profile" component={UserProfile} />
            <Route path="*" component={NotFound} />
          </Switch>
        </AuthContext.Provider>
      </BrowserRouter>
    </div>
  );
};

export default App;
